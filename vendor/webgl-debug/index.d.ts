//----------------------------------------------------------------------------------------
//  Ambient type declarations for WebGLDebugUtils
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
//  The easiest way to get going with a 3rd party JS lib:
//    - Include a script tag in the the HTML file
//    - In each TypeScript module that uses the library:
//      - Declare a variable for the library with the "any" type
//      - `declare var WebGLDebugUtils: any`

//----------------------------------------------------------------------------------------
//  I initially attempted to use TypeScripts "import" keyword.  These definitions
//  satisfied the typechecker, but it failed to resovle the module at runtime.  In all
//  cases I used:
//    `import * as WebGLDebugUtils from '../vendor/webgl-debug'`

// export var makeDebugContext: any
// export var glEnumToString: any
// export var glFunctionArgsToString: any

//----------------------------------------------------------------------------------------
//  The following declaration works when:
//    - Including the library in a script tag
//    - Importing this declaration file with a triple-slash directive
//      - Before any non-comment lines in the client TypeScript file
//      - `/// <reference types="../vendor/webgl-debug" />

// declare var WebGLDebugUtils: any

//----------------------------------------------------------------------------------------
//  To improve on the previous declaration, I introduce the library's member names

interface WebGLDebugUtils {
    makeDebugContext: any
    glEnumToString: any
    glFunctionArgsToString: any
}
declare var WebGLDebugUtils: WebGLDebugUtils