const path = require('path')
const webpack = require('webpack')

module.exports = env => {
    env = Object.assign({
        production: false,
    }, env ? env : {})

    let options = {
        mode: env.production ? 'production' : 'development',
        entry: './src/index.ts',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.ts?$/,
                    loader: 'awesome-typescript-loader',
                    exclude: /node_modules/,
                },
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    loader: 'source-map-loader',
                },
            ],
        },
        resolve: {
            extensions: ['.ts', '.js'],
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        plugins: [
            new webpack.ProvidePlugin({
                // glMatrix: '../vendor/gl-matrix',
                mat4: ['../vendor/gl-matrix', 'mat4'],
            }),
            // new webpack.DefinePlugin({
            //     WebGLDebugUtils: undefined,
            // }),
        ],
    }

    // if (! env.production) {
    //     options.plugins.push(new webpack.ProvidePlugin({
    //         WebGLDebugUtils: '../vendor/webgl-debug',
    //     }))
    // }

    return options
}