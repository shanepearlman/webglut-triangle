//----------------------------------------------------------------------------------------
//  A minimal WebGL application using WebGLUT
//----------------------------------------------------------------------------------------

/// <reference types="../vendor/webgl-debug" />
/// <reference types="../vendor/gl-matrix" />

import { gfx, util } from 'webglut'
import { throwOnGLError, logAndValidate } from './util'

if (util.isDefined(WebGLDebugUtils)) {
    // Ensure no WebGL library calls are made until the WebGL Inspector is ready
    window.addEventListener('gliready', main)
} else {
    document.addEventListener('DOMContentLoaded', main)
}

function main() {
    let canvas = document.getElementById('glCanvas')
    if (! (canvas instanceof HTMLCanvasElement)) {
        throw 'DOM element #glCanvas is not an HTMLCanvasElement'
    }

    let gl = canvas.getContext('webgl')
    if (gl === null) {
        throw 'Failed to get WebGLRenderingContext'
    }
    if (util.isDefined(WebGLDebugUtils)) {
        gl = WebGLDebugUtils.makeDebugContext(gl, throwOnGLError, logAndValidate)
        gl = gl as WebGLRenderingContext
    }

    canvas.width = canvas.clientWidth
    canvas.height = canvas.clientWidth

    renderScene(gl)
}

function renderScene(gl: WebGLRenderingContext) {
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)

    gl.clearColor(0.0, 0.0, 0.0, 1.0)
    gl.clearDepth(1.0)
    gl.enable(gl.DEPTH_TEST)
    gl.depthFunc(gl.LEQUAL)

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    let trigPipeline = makeTrigPipeline(gl)
    gfx.usePipeline(gl, trigPipeline)

    let uniformLocations = trigPipeline.programInfo.uniformLocations
    setTrigUniforms(gl, uniformLocations)

    gfx.render(gl, trigPipeline)
}

function setTrigUniforms(
    gl: WebGLRenderingContext, uniformLocations: util.Dictionary<WebGLUniformLocation>)
{
    let projLocation = uniformLocations.projectionMatrix
    if (! util.isDefined(projLocation)) {
        throw 'Uniform location is undefined'
    }
    let proj = makePerspectiveProjectionMatrix(gl)
    gl.uniformMatrix4fv(projLocation, false, proj)

    let modelViewLocation = uniformLocations.modelViewMatrix
    if (! util.isDefined(modelViewLocation)) {
        throw 'Uniform location is undefined'
    }
    let modelView = mat4.create()
    mat4.translate(modelView, modelView, [0.0, 0.0, -6.0])
    gl.uniformMatrix4fv(modelViewLocation, false, modelView)
}

function makePerspectiveProjectionMatrix(gl: WebGLRenderingContext) {
    let fieldOfView = 45 * Math.PI / 180
    let aspect = gl.canvas.width / gl.canvas.height
    let zNear = 0.1
    let zFar = 100.0
    let projectionMatrix = mat4.create()
    mat4.perspective(projectionMatrix, fieldOfView, aspect, zNear, zFar)
    return projectionMatrix
}

function makeTrigState(gl: WebGLRenderingContext): gfx.State {

    //  positions       colors
    let attribs = [
        -1.0, -1.0,     0.8, 0.2, 0.2,
         1.0, -1.0,     0.2, 0.8, 0.2,
         0.0,  1.0,     0.2, 0.2, 0.8,
    ]

    return {
        attribCount: 3,
        attribOffset: 0,
        attributes: {
            positions: {
                data: attribs,
                layout: {
                    size: 2,
                    type: gl.FLOAT,
                    stride: 5 * 4,
                    offset: 0,
                },
            },
            colors: {
                data: attribs,
                layout: {
                    size: 3,
                    type: gl.FLOAT,
                    stride: 5 * 4,
                    offset: 2 * 4,
                },
            },
        },
    }
}

function makeTrigPipeline(gl: WebGLRenderingContext): gfx.Pipeline {
    return {
        programInfo: makeTrigShader(gl),
        state: makeTrigState(gl),
        dataMap: {
            positions: 'vertexPosition',
            colors: 'vertexColor',
        },
    }
}

function makeTrigShader(gl: WebGLRenderingContext): gfx.ProgramInfo {
    let program = gfx.makeShaderProgram(gl, trigVS, trigFS)

    return {
        program: program,
        attribLocations: {
            vertexPosition: 'aVertexPosition',
            vertexColor: 'aVertexColor',
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
        },
    }
}

const trigVS = `
    attribute vec4 aVertexPosition;
    attribute vec4 aVertexColor;
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;
    varying vec4 vColor;
    void main() {
        gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
        vColor = aVertexColor;
    }
`

const trigFS = `
    precision mediump float;
    varying vec4 vColor;
    void main() {
        gl_FragColor = vColor;
    }
`