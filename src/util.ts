/// <reference types="../vendor/webgl-debug" />

export function throwOnGLError(err: number, funcName: string, args: any) {
    throw WebGLDebugUtils.glEnumToString(err) + ' was caused by call to: ' + funcName
}

export function logGLCall(funcName: string, args: any) {   
    args = WebGLDebugUtils.glFunctionArgsToString(funcName, args)
    console.log('gl.' + funcName + '(' + args + ')')
}

export function checkUndefinedArgs(functionName: string, args: any) {
    for (var i = 0; i < args.length; i++) {
        if (args[i] === undefined) {
            args = WebGLDebugUtils.glFunctionArgsToString(functionName, args)
            console.error('undefined passed to gl.' + functionName + '(' + args + ')')
            return
        }
    }
}

export function logAndValidate(functionName: string, args: any) {
    logGLCall(functionName, args)
    checkUndefinedArgs(functionName, args)
}